import lombok.Data;

@Data
public class Game {
    private Machine machine;
    private Player player1;
    private Player player2;
    private int rounds;

    public Game(Machine machine, Player player1, Player player2, int rounds) {
        this.machine = machine;
        this.player1 = player1;
        this.player2 = player2;
        this.rounds = rounds;
    }

    public ScoreModel startGame() {
        for(int i=0; i<rounds; i++){
            Move player1Move = player1.move();
            Move player2Move = player2.move();
            ScoreModel scoreModel = machine.calculateScore(player1Move, player2Move);
            player1.addScore(scoreModel.getPlayer1Score());
            player2.addScore(scoreModel.getPlayer2Score());
            printScore(player1.getScore(), player2.getScore());
        }

        return new ScoreModel(player1.getScore(), player2.getScore());
    }

    private void printScore(int player1Score, int player2Score){
        System.out.println("Score: Player1: " + player1Score + " Player2: " + player2Score);
    }
}
