import lombok.Data;


@Data
public class Player {

    private static final String COOPERATION_MOVE = "COOP";

    private int id;
    private int score;
    private Movable movable;

    public Player(int id, Movable movable, int score) {
        this.id = id;
        this.movable = movable;
        this.score = score;
    }

    public Move move() {
        return movable.move();
    }

    public void addScore(int points) {
        score += points;
    }
}
