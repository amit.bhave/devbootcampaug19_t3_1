import java.util.Scanner;

public class GameController {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Movable consoleMovable = new ConsoleMovable(scanner);
        Game game = new Game(new Machine(), new Player(1, () -> Move.COOP, 0), new Player(2, () -> Move.CHEAT, 0), 5);
        ScoreModel model = game.startGame();
        System.out.println("Final score: " + model.getPlayer1Score() + " : " + model.getPlayer2Score());
    }
}
