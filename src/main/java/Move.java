public enum Move {

    COOP("COOP"), CHEAT("CHEAT");

    private String moveType;

    Move(String moveType) {
        this.moveType = moveType;
    }

    public String getMoveType() {
        return moveType;
    }

}
