import java.util.Scanner;

public class ConsoleMovable implements Movable{

    private final Scanner scanner;

    public ConsoleMovable(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public Move move() {
        return Move.valueOf(scanner.next().toUpperCase());
    }
}
