public class Machine {

    public ScoreModel calculateScore(Move player1Move, Move player2Move) {
        ScoreModel scoreModel = new ScoreModel(0,0);
        if(Move.COOP == player1Move && Move.COOP == player2Move){
            scoreModel.setPlayer1Score(2);
            scoreModel.setPlayer2Score(2);
        } else if(Move.COOP == player1Move && Move.CHEAT == player2Move){
            scoreModel.setPlayer1Score(-1);
            scoreModel.setPlayer2Score(3);
        } else if(Move.CHEAT == player1Move && Move.COOP == player2Move){
            scoreModel.setPlayer1Score(3);
            scoreModel.setPlayer2Score(-1);
        }

        return scoreModel;
    }
}
