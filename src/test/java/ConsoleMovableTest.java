import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Scanner;

public class ConsoleMovableTest {

    @Test
    public void shouldBeCheatMove() {
        Scanner sc = new Scanner(Move.CHEAT.getMoveType());
        ConsoleMovable consoleMovable = new ConsoleMovable(sc);
        Move move = consoleMovable.move();
        Assert.assertEquals(Move.CHEAT, move);
    }

    @Test
    public void shouldBeCoopMove() {
        Scanner sc = new Scanner(Move.COOP.getMoveType());
        ConsoleMovable consoleMovable = new ConsoleMovable(sc);
        Move move = consoleMovable.move();
        Assert.assertEquals(Move.COOP, move);
    }
}
