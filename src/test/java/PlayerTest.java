import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {

    @Test
    public void shouldHandleCooperateMove(){
        Scanner sc = new Scanner(Move.COOP.getMoveType());
        Player player = new Player(1, new ConsoleMovable(sc), 0);
        Move move = player.move();
        Assert.assertEquals(Move.COOP, move);
    }

    @Test
    public void shouldHandleCheatingMove(){
        Scanner sc = new Scanner(Move.CHEAT.getMoveType());
        Player player = new Player(1, new ConsoleMovable(sc), 0);
        Move move = player.move();
        Assert.assertEquals(Move.CHEAT, move);
    }

}
