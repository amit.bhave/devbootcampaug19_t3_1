import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class GameTest {

    private Game game;
    private Player player1;
    private Player player2;
    private Machine machine;

    @Before
    public void setUp(){
        player1 = Mockito.mock(Player.class);
        player2 = Mockito.mock(Player.class);
        machine = Mockito.mock(Machine.class);
        game = new Game(machine, player1, player2, 1);
    }

    @Test
    public void shouldStartGame(){
        //Act
        when(player1.move()).thenReturn(Move.COOP);
        when(player2.move()).thenReturn(Move.CHEAT);
        when(machine.calculateScore(Move.COOP, Move.CHEAT)).thenReturn(new ScoreModel(-1, 3));
        ScoreModel scoreModel = game.startGame();

        //Assert
        Mockito.verify(player1).move();
        Mockito.verify(player2).move();
        Mockito.verify(machine).calculateScore(Move.COOP, Move.CHEAT);
    }

    @Test
    public void shouldStartGameFor5Rounds(){

        Movable movable1 = mock(Movable.class);
        Movable movable2 = mock(Movable.class);
        player1 = new Player(1, movable1, 0);
        player2 = new Player(2, movable2, 0);
        game = new Game(machine, player1, player2, 5);


        //Act
        when(movable1.move()).thenReturn(Move.COOP);
        when(movable2.move()).thenReturn(Move.CHEAT);
        when(machine.calculateScore(Move.COOP, Move.CHEAT)).thenReturn(new ScoreModel(-1, 3));
        ScoreModel scoreModel = game.startGame();

        //Assert
        Mockito.verify(movable1, times(5)).move();
        Mockito.verify(movable2, times(5)).move();
        Mockito.verify(machine, times(5)).calculateScore(Move.COOP, Move.CHEAT);
    }
}
