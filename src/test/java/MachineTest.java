import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Scanner;

public class MachineTest {

    private Machine machine;
    private Player player1;
    private Player player2;

    @Before
    public void shouldReturnInstance(){
        machine = new Machine();
        Scanner scanner = new Scanner("");
        player1 = new Player(1, new ConsoleMovable(scanner), 0);
        player2 = new Player(2, new ConsoleMovable(scanner), 0);
    }

    @Test
    public void shouldCalculateScoreForCoopAndCoopCase(){
        ScoreModel scoreModel = machine.calculateScore(Move.COOP, Move.COOP);
        Assert.assertEquals(2, scoreModel.getPlayer1Score());
        Assert.assertEquals(2, scoreModel.getPlayer2Score());
    }

    @Test
    public void shouldCalculateScoreForCoopAndCheatCase(){
        ScoreModel scoreModel = machine.calculateScore(Move.COOP, Move.CHEAT);
        Assert.assertEquals(-1, scoreModel.getPlayer1Score());
        Assert.assertEquals(3, scoreModel.getPlayer2Score());
    }

    @Test
    public void shouldCalculateScoreForCheatAndCoopCase(){
        ScoreModel scoreModel = machine.calculateScore(Move.CHEAT, Move.COOP);
        Assert.assertEquals(3, scoreModel.getPlayer1Score());
        Assert.assertEquals(-1, scoreModel.getPlayer2Score());
    }

    @Test
    public void shouldCalculateScoreForCheatAndCheatCase(){
        ScoreModel scoreModel = machine.calculateScore(Move.CHEAT, Move.CHEAT);
        Assert.assertEquals(0, scoreModel.getPlayer1Score());
        Assert.assertEquals(0, scoreModel.getPlayer2Score());
    }
}
